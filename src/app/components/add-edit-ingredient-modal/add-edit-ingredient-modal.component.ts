import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/modal';
import {NgForm} from '@angular/forms';
import {IngredientService} from "../../services/ingredient.service";
import {Ingredient} from "../../interfaces/ingredient.interface";

@Component({
    selector: 'add-edit-ingredient-modal',
    templateUrl: './add-edit-ingredient-modal.component.html',
    styleUrls: ['./add-edit-ingredient-modal.component.css']
})
export class AddEditIngredientModalComponent implements OnInit {
    @Input() public modalTitle: string;
    @ViewChild('ingredientModal') public ingredientModal: ModalDirective;
    @ViewChild('form') public form: NgForm;

    public categories: string[] = [
        'Vegetable',
        'Fruit',
        'Protein',
        'Fat',
        'Grain',
        'Condiment',
        'Dressing'
    ];
    public selectedIngredient: Ingredient = {
        id: null,
        name: '',
        category: '',
        metric: '',
        amount: null
    };

    constructor(private ingredientService: IngredientService) { }
    ngOnInit() { }

    public showModal(type: string): void {
        if (type === 'add') {
            this.modalTitle = 'Add Ingredient';
        } else {
            this.modalTitle = 'Edit Ingredient';
            this.selectedIngredient.id = this.getId();
        }

        console.log(this.selectedIngredient);
        this.ingredientModal.show();
    }

    public hideModal(): void {
        this.ingredientModal.hide();
    }

    getId(): number {
        return this.ingredientService.getIngredientId(this.selectedIngredient);
    }

    validateName(): boolean {
        return (
            !!this.selectedIngredient.name &&
            !!this.selectedIngredient.name.trim()
        );
    }

    categoryChange(newValue: string): void {
        this.selectedIngredient.category = newValue;
    }

    metricChange(newValue: string): void {
        this.selectedIngredient.metric = newValue;
    }

    amountChange(newValue: number): void {
        this.selectedIngredient.amount = newValue;
    }

    saveIngredient(): void {
        if (this.modalTitle === 'Add Ingredient') {
            this.ingredientService.saveIngredient(
                this.selectedIngredient.name,
                this.selectedIngredient.category,
                this.selectedIngredient.metric,
                this.selectedIngredient.amount
            );
        }
        else if (this.modalTitle === 'Edit Ingredient') {
            this.ingredientService.updateIngredient(this.selectedIngredient);
        }

        this.hideModal();
    }

    clearForm() {
        // this.form.resetForm();
    }
}
