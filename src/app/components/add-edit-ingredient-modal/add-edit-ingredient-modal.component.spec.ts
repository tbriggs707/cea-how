import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditIngredientModalComponent } from './add-edit-ingredient-modal.component';

describe('AddEditIngredientModalComponent', () => {
  let component: AddEditIngredientModalComponent;
  let fixture: ComponentFixture<AddEditIngredientModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditIngredientModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditIngredientModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
