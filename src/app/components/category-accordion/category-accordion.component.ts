import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {IngredientService} from "../../services/ingredient.service";
import {Ingredient} from "../../interfaces/ingredient.interface";
import {AddEditIngredientModalComponent} from "../add-edit-ingredient-modal/add-edit-ingredient-modal.component";

@Component({
    selector: 'category-accordion',
    templateUrl: './category-accordion.component.html',
    styleUrls: ['./category-accordion.component.css']
})
export class CategoryAccordionComponent implements OnInit {
    @Input() public categoryName: string;
    @ViewChild(AddEditIngredientModalComponent) ingredientModal: AddEditIngredientModalComponent;
    public isExpanded: boolean = false;
    public ingredients: Ingredient[];

    constructor(private ingredientService: IngredientService) {
    }

    ngOnInit() {
        this.ingredients = this.ingredientService.getIngredientsByCategory(this.categoryName);
    }

    openModal(ingredient: Ingredient): void {
        this.ingredientModal.selectedIngredient.name = ingredient.name;
        this.ingredientModal.selectedIngredient.category = ingredient.category;
        this.ingredientModal.selectedIngredient.metric = ingredient.metric;
        this.ingredientModal.selectedIngredient.amount = ingredient.amount;
        this.ingredientModal.showModal('edit');
    }

    delete(ingredient: Ingredient): void {
        //TODO: add confirmation popover/modal
        // http://bootstrap-confirmation.js.org/
        this.ingredientService.deleteIngredient(ingredient);
    }

    toggleExpand(): void {
        this.isExpanded = !this.isExpanded;
    }

    getPlural(categoryName: string): string {
        return categoryName + 's';
    }

}
