import {Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {Ingredient} from "../../interfaces/ingredient.interface";
import {Categories} from "../../interfaces/categories.interface";
import {IngredientService} from "../../services/ingredient.service";
import {AddEditIngredientModalComponent} from "../add-edit-ingredient-modal/add-edit-ingredient-modal.component";

@Component({
    selector: 'ingredients-list',
    templateUrl: './ingredients-list.component.html',
    styleUrls: ['./ingredients-list.component.css'],
    providers: [IngredientService]
})
export class IngredientsListComponent implements OnInit, AfterViewInit {
    @ViewChild(AddEditIngredientModalComponent) ingredientModal: AddEditIngredientModalComponent;
    public categories: Categories = {
        fruit: {name: 'Fruit', isExpanded: false},
        vegetable: {name: 'Vegetable', isExpanded: false},
        fat: {name: 'Fat', isExpanded: false},
        dressing: {name: 'Dressing', isExpanded: false},
        grain: {name: 'Grain', isExpanded: false},
        protein: {name: 'Protein', isExpanded: false},
        condiment: {name: 'Condiment', isExpanded: false}
    };

    expand(category: string): void {
        switch (category) {
            case 'Fruit':
                this.categories.fruit.isExpanded = !this.categories.fruit.isExpanded;
                break;

            case 'Vegetable':
                this.categories.vegetable.isExpanded = !this.categories.vegetable.isExpanded;
                break;

            case 'Protein':
                this.categories.protein.isExpanded = !this.categories.protein.isExpanded;
                break;

            case 'Fat':
                this.categories.fat.isExpanded = !this.categories.fat.isExpanded;
                break;

            case 'Condiment':
                this.categories.condiment.isExpanded = !this.categories.condiment.isExpanded;
                break;

            case 'Dressing':
                this.categories.dressing.isExpanded = !this.categories.dressing.isExpanded;
                break;

            case 'Grain':
                this.categories.grain.isExpanded = !this.categories.grain.isExpanded;
                break;
        }
    }

    openModal(): void {
        this.ingredientModal.showModal('add');
    }

    constructor() {}

    ngOnInit() {}

    ngAfterViewInit() {}

}
