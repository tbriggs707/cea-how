import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AlertModule} from 'ng2-bootstrap';
import { ModalModule } from 'ng2-bootstrap';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import {routing} from "./app.routes";

// used to create fake backend
import {fakeBackendProvider} from './helpers/index';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {BaseRequestOptions} from '@angular/http';
import {AuthGuard} from "./guards/auth.guard";
import {AuthenticationService} from "./services/authentication.service";
import {UserService} from "./services/user.service";
import {AlertService} from "./services/alert.service";
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { IngredientsListComponent } from './components/ingredients-list/ingredients-list.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { CategoryAccordionComponent } from './components/category-accordion/category-accordion.component';
import { AddEditIngredientModalComponent } from './components/add-edit-ingredient-modal/add-edit-ingredient-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    CreateAccountComponent,
    ForgotPasswordComponent,
    NavBarComponent,
    IngredientsListComponent,
    HomePageComponent,
    CategoryAccordionComponent,
    AddEditIngredientModalComponent
  ],
  imports: [
        AlertModule.forRoot(),
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        ModalModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    UserService,
    AlertService,

    // providers used to create fake backend
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
