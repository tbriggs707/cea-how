import {ModuleWithProviders}  from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CreateAccountComponent} from "./components/create-account/create-account.component";
import {ForgotPasswordComponent} from "./components/forgot-password/forgot-password.component";
import {HomePageComponent} from "./components/home-page/home-page.component";
import {LandingPageComponent} from "./components/landing-page/landing-page.component";

import {AuthGuard} from './guards/index';
import {NavBarComponent} from "./components/nav-bar/nav-bar.component";
import {IngredientsListComponent} from "./components/ingredients-list/ingredients-list.component";

// Route Configuration
export const appRoutes: Routes = [
    {path: 'login', component: LandingPageComponent},
    {path: '', component: HomePageComponent, canActivate: [AuthGuard]},
    {path: 'home', component: HomePageComponent, canActivate: [AuthGuard]},
    {path: 'create-account', component: CreateAccountComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent},
    {path: 'ingredients', component: IngredientsListComponent},

    {path: '**', component: LandingPageComponent} // change to 404, not found
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
