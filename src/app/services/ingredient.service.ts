/**
 * Created by tbriggs on 2/11/2017.
 */

import {Injectable} from "@angular/core";
import {Ingredient} from "../interfaces/ingredient.interface";

@Injectable()
export class IngredientService {
    public ingredients = {
        "Vegetable": [
            {
                id: 3,
                name: 'Artichokes',
                category: 'Vegetable',
                metric: 'Ounces (oz)',
                amount: 8
            },
            {
                id: 20,
                name: 'Carrots',
                category: 'Vegetable',
                metric: 'Ounces (oz)',
                amount: 8
            }
        ],
        "Fruit": [
            {
                id: 1,
                name: 'Apple',
                category: 'Fruit',
                metric: 'Whole',
                amount: 1
            },
            {
                id: 2,
                name: 'Apricot',
                category: 'Fruit',
                metric: 'Whole',
                amount: 1
            }
        ],
        "Protein": [
            {
                id: 86,
                name: 'Yogurt',
                category: 'Protein',
                metric: 'Ounces (oz)',
                amount: 8
            }
        ],
        "Grain": [
            {
                id: 28,
                name: 'Corn',
                category: 'Grain',
                metric: 'Ounces (oz)',
                amount: 4
            }
        ],
        "Fat": [
            {
                id: 5,
                name: 'Avocado',
                category: 'Fat',
                metric: 'Whole',
                amount: 0.125
            }
        ],
        "Dressing": [
            {
                id: 70,
                name: 'Ranch',
                category: 'Dressing',
                metric: 'Table Spoons (Tbsp)',
                amount: 2
            }
        ],
        "Condiment": [
            {
                id: 45,
                name: 'Ketchup',
                category: 'Condiment',
                metric: 'Cups (c)',
                amount: 0.5
            }
        ]
    };

    getIngredientsByCategory(category: string): Ingredient[] {
        return this.ingredients[category];
    }

    saveIngredient(name: string, category: string, metric: string, amount: number): void {
        //TODO: use API to create a new ingredient record with auto-generated id
        this.ingredients[category].push({
            id: 99,
            name: name,
            category: category,
            metric: metric,
            amount: amount
        });
    }

    updateIngredient(updatedIngredient: Ingredient): void {
        //TODO: send through API to update record with same id

        for (let ing of this.ingredients[updatedIngredient.category]) {
            if (ing.id === updatedIngredient.id) {
                ing.name = updatedIngredient.name;
                ing.category = updatedIngredient.category;
                ing.metric = updatedIngredient.metric;
                ing.amount = updatedIngredient.amount;
                return;
            }
        }
    }

    deleteIngredient(ingredient: Ingredient): void {
        //TODO: send through API to delete record from DB

        let ingredients = this.ingredients[ingredient.category];
        for (let i in ingredients) {
            if (ingredients[i].id === ingredient.id) {
                ingredients.splice(i, 1);
            }
        }
    }

    getIngredientId(ingredient: Ingredient): number {
        let name = ingredient.name;
        let category = ingredient.category;

        for (let ing of this.ingredients[category]) {
            if (ing.name === name) {
                return ing.id;
            }
        }
    }
}