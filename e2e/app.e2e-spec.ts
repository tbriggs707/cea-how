import { CeaHowPage } from './app.po';

describe('cea-how App', () => {
  let page: CeaHowPage;

  beforeEach(() => {
    page = new CeaHowPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
